package com.ad2pro.spectra.reporting.deserializer;

import com.ad2pro.spectra.reporting.exceptions.ConsumerException;
import com.ad2pro.spectra.reporting.utils.ObjectMapperUtility;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

/**
 * Created By: naveen
 * Date: 24/01/19
 * Package: com.ad2pro.spectra.reporting.domains
 * Sample for Deserializer
 **/
@Data
@Slf4j
public class SampleCustomDeserializer implements Deserializer {

    @Override
    public Object deserialize(String s, byte[] bytes) {
        SampleClass sampleClass;
        try {
            sampleClass = ObjectMapperUtility.getObjectMapper().readValue(bytes, SampleClass.class);
        } catch (Exception e) {
            log.error("Exceptions Occurred {}. ", e.getMessage());
            throw new ConsumerException(e.getMessage());
        }
        return sampleClass;
    }

    @Override
    public void configure(Map map, boolean b) {

    }

    @Override
    public void close() {

    }

    private static class SampleClass {
        private Integer a;
        private String b;
    }
}
