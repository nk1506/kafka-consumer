package com.ad2pro.spectra.reporting.exceptions;

/**
 * Created By: naveen
 * Date: 28/01/19
 * Package: com.ad2pro.spectra.reporting.exceptions
 **/
public class ConsumerException extends RuntimeException {

    public ConsumerException(String message) {
        super(message);
    }
}
