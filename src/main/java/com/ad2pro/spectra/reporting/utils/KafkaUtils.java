package com.ad2pro.spectra.reporting.utils;

import com.ad2pro.spectra.reporting.exceptions.ConsumerException;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created By: naveen
 * Date: 24/01/19
 * Package: com.ad2pro.spectra.reporting.utils
 **/
@Slf4j
public class KafkaUtils {

    private KafkaUtils() {
    }

    public static Properties consumerProperties(Deserializer keyDeserializer, Deserializer valueDeserializer) {

        final Properties consumerProperties = new Properties();
        Properties props = new Properties();
        InputStream input = null;
        try {
            String filename = "kafka-consumer.properties";
            input = KafkaUtils.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                log.error("Sorry, unable to find properties file {} . ", filename);
                throw new ConsumerException("Properties File Not Found!");
            }
            //load a properties file from class path, inside static method
            props.load(input);

            consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, props.getProperty("kafka.brokers"));
            consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, keyDeserializer.getClass());
            consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, valueDeserializer.getClass());
            consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, props.getProperty("consumer.groupId"));
            consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        } catch (IOException ex) {
            log.error("Exceptions Occurred while reading Kafka Properties.");
            throw new ConsumerException(ex.getMessage());
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    log.error("Exceptions {} Occurred while closing the input file.",e.getMessage());
                }
            }
        }
        return consumerProperties;
    }
}
