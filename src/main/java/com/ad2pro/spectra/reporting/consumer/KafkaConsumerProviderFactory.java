package com.ad2pro.spectra.reporting.consumer;

import com.ad2pro.spectra.reporting.utils.KafkaUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created By: naveen
 * Date: 29/01/19
 * Package: com.ad2pro.spectra.reporting.consumer.impl
 **/

@Slf4j
public class KafkaConsumerProviderFactory {

    public KafkaConsumer getTopicsKafkaConsumer(List<String> topics, Deserializer keyDeserializer, Deserializer valueDeserializer) {
        KafkaConsumer consumer = new KafkaConsumer<>(KafkaUtils.consumerProperties(keyDeserializer, valueDeserializer));
        consumer.subscribe(topics);
        return consumer;
    }

    public KafkaConsumer getTopicKafkaConsumer(String topic, int partition, long offset, Deserializer keyDeserializer, Deserializer valueDeserializer) {
        KafkaConsumer consumer = new KafkaConsumer<>(KafkaUtils.consumerProperties(keyDeserializer, valueDeserializer));
        TopicPartition topicPartition = new TopicPartition(topic,partition);
        consumer.assign(Collections.singleton(topicPartition));
        consumer.seek(topicPartition, offset);
        return consumer;
    }

    public KafkaConsumer getTopicKafkaConsumer(String topic, long startingOffset, Deserializer keyDeserializer, Deserializer valueDeserializer) {
        KafkaConsumer consumer = new KafkaConsumer<>(KafkaUtils.consumerProperties(keyDeserializer, valueDeserializer));
        consumer.subscribe(Collections.singleton(topic), new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                //Commit Offset, Executes after consumer
                log.info("Consumer has revoked it after consuming event. ", consumer);
            }
            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                //Assign Partition and Offset for consumer, Executes before consume
                Iterator<TopicPartition> topicPartitionIterator = partitions.iterator();
                while(topicPartitionIterator.hasNext()){
                    TopicPartition topicPartition = topicPartitionIterator.next();
                    log.info("Current offset is {} committed offset is -> {} ",
                            consumer.position(topicPartition), consumer.committed(topicPartition) );
                    if(startingOffset == -2) {
                        log.info("Ignoring it, Using Default Offset value!");
                    } else if(startingOffset ==0){
                        log.info("Setting offset from beginning");
                        consumer.seekToBeginning(partitions);
                    } else if(startingOffset == -1){
                        log.info("Setting offset to the end ");
                        consumer.seekToEnd(partitions);
                    } else {
                        log.info("Starting events from offset {} ", startingOffset);
                        consumer.seek(topicPartition, startingOffset);
                    }
                }
            }
        });
        return consumer;
    }

    public void closeKafkaConsumer(KafkaConsumer consumer) {
        consumer.close();
    }
}
