package com.ad2pro.spectra.reporting;

import com.ad2pro.spectra.reporting.consumer.KafkaConsumerProviderFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By: naveen
 * Date: 29/01/19
 * Package: com.ad2pro.spectra.reporting
 **/
public class Example {

    private static void pollWithTopics() {
        KafkaConsumerProviderFactory kafkaConsumer = new KafkaConsumerProviderFactory();
        List<String> topics = new ArrayList<>();
        topics.add("topic-name");
        KafkaConsumer kafkaConsumer1 = kafkaConsumer.getTopicsKafkaConsumer(topics, new StringDeserializer(), new StringDeserializer());
        while (true) {
            ConsumerRecords<String, String> messages = kafkaConsumer1.poll(Duration.ofSeconds(10));
            for (ConsumerRecord<String, String> message : messages) {
                System.out.println("Offset received " + message.offset());
                System.out.println("Key received " + message.key());
                System.out.println("Value received " + message.value());
            }
        }
    }

    private static void pollWithTopic() {
        KafkaConsumerProviderFactory kafkaConsumer = new KafkaConsumerProviderFactory();
        String topic = "kafka-local-topic-4";
        KafkaConsumer kafkaConsumer1 = kafkaConsumer.getTopicKafkaConsumer(topic, 100, new StringDeserializer(), new StringDeserializer());
        while (true) {
            ConsumerRecords<String, String> messages = kafkaConsumer1.poll(Duration.ofSeconds(10));
            for (ConsumerRecord<String, String> message : messages) {
                System.out.println("Offset received " + message.offset());
                System.out.println("Message received " + message.value());
                System.out.println("key received " + message.key());

            }
        }
    }
}
